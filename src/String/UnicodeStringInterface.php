<?php

declare(strict_types=1);

namespace C33s\Utils\String;

/**
 * Interface which represent the current (5.x 99f25957efe05db14a1aa6cff643eca0f83a952c) signature of the UnicodeString
 * class. Used to backport some methods to php 7.1.
 */
interface UnicodeStringInterface
{
//    /**
//     * Unwraps instances of AbstractString back to strings.
//     *
//     * @return string[]|array
//     */
//    public static function unwrap(array $values): array;
//
//    /**
//     * Wraps (and normalizes) strings in instances of AbstractString.
//     *
//     * @return static[]|array
//     */
//    public static function wrap(array $values): array;
//
//    /**
//     * @param string|string[] $needle
//     *
//     * @return static
//     */
//    public function after($needle, bool $includeNeedle = false, int $offset = 0): \Symfony\Component\String\UnicodeString;
//
//    /**
//     * @param string|string[] $needle
//     *
//     * @return static
//     */
//    public function afterLast($needle, bool $includeNeedle = false, int $offset = 0): \Symfony\Component\String\UnicodeString;
//
//    /**
//     * @param string|string[] $needle
//     *
//     * @return static
//     */
//    public function before($needle, bool $includeNeedle = false, int $offset = 0): \Symfony\Component\String\UnicodeString;
//
//    /**
//     * @param string|string[] $needle
//     *
//     * @return static
//     */
//    public function beforeLast($needle, bool $includeNeedle = false, int $offset = 0): \Symfony\Component\String\UnicodeString;
//
//    /**
//     * @return int[]
//     */
//    public function bytesAt(int $offset): array;
//
//    /**
//     * @return static
//     */
//    public function collapseWhitespace(): \Symfony\Component\String\UnicodeString;
//
//    /**
//     * @param string|string[] $needle
//     */
//    public function containsAny($needle): bool;
//
//    /**
//     * @return static
//     */
//    public function ensureEnd(string $suffix): \Symfony\Component\String\UnicodeString;
//
//    /**
//     * @return static
//     */
//    public function ensureStart(string $prefix): \Symfony\Component\String\UnicodeString;
//
//    /**
//     * @return static
//     */
//    public function ignoreCase(): \Symfony\Component\String\UnicodeString;
//
//    public function isEmpty(): bool;
//
//    public function jsonSerialize(): string;
//
//    /**
//     * @return static
//     */
//    public function repeat(int $multiplier): \Symfony\Component\String\UnicodeString;
//
//    public function toByteString(string $toEncoding = null): ByteString;
//
//    public function toCodePointString(): CodePointString;
//
//    public function toString(): string;
//
//    public function toUnicodeString(): UnicodeString;
//

    /**
     * @return static
     */
    public function truncate(int $length, string $ellipsis = '', bool $cut = true);

//
//    /**
//     * @return static
//     */
//    public function wordwrap(int $width = 75, string $break = "\n", bool $cut = false): \Symfony\Component\String\UnicodeString;
//
//    public function __sleep(): array;
//
    public function __toString(): string;

//
//    /**
//     * @return static
//     */
//    public static function fromCodePoints(int ...$codes): \Symfony\Component\String\UnicodeString;
//
//    /**
//     * Generic UTF-8 to ASCII transliteration.
//     *
//     * Install the intl extension for best results.
//     *
//     * @param string[]|\Transliterator[]|\Closure[] $rules See "*-Latin" rules from Transliterator::listIDs()
//     */
//    public function ascii(array $rules = []): \Symfony\Component\String\UnicodeString;
//
//    public function camel(): \Symfony\Component\String\AbstractUnicodeString;
//
//    /**
//     * @return int[]
//     */
//    public function codePointsAt(int $offset): array;
//
//    public function folded(bool $compat = true): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function lower(): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function match(string $regexp, int $flags = 0, int $offset = 0): array;
//
//    public function padBoth(int $length, string $padStr = ' '): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function padEnd(int $length, string $padStr = ' '): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function padStart(int $length, string $padStr = ' '): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function reverse(): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function snake(): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function title(bool $allWords = false): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function trim(string $chars = " \t\n\r\0\x0B\x0C\u{A0}\u{FEFF}"): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function trimEnd(string $chars = " \t\n\r\0\x0B\x0C\u{A0}\u{FEFF}"): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function trimStart(string $chars = " \t\n\r\0\x0B\x0C\u{A0}\u{FEFF}"): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function upper(): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function width(bool $ignoreAnsiDecoration = true): int;
//
//    /**
//     * @return static
//     */
//    public function pad(int $len, self $pad, int $type): \Symfony\Component\String\AbstractUnicodeString;
//
//    /**
//     * Based on https://github.com/jquast/wcwidth, a Python implementation of https://www.cl.cam.ac.uk/~mgk25/ucs/wcwidth.c.
//     */
//    public function wcswidth(string $string): int;
//
//    public function __construct(string $string = '');
//
//    public function append(string ...$suffix): AbstractString;
//
//    public function chunk(int $length = 1): array;
//
//    public function endsWith($suffix): bool;
//
//    public function equalsTo($string): bool;
//
//    public function indexOf($needle, int $offset = 0): ?int;
//
//    public function indexOfLast($needle, int $offset = 0): ?int;
//
//    public function join(array $strings, string $lastGlue = null): AbstractString;
//
//    public function length(): int;
//
//    /**
//     * @return static
//     */
//    public function normalize(int $form = self::NFC): \Symfony\Component\String\AbstractUnicodeString;
//
//    public function prepend(string ...$prefix): AbstractString;
//
//    public function replace(string $from, string $to): AbstractString;
//
//    public function replaceMatches(string $fromRegexp, $to): AbstractString;
//
//    public function slice(int $start = 0, int $length = null): AbstractString;
//
//    public function splice(string $replacement, int $start = 0, int $length = null): AbstractString;
//
//    public function split(string $delimiter, int $limit = null, int $flags = null): array;
//
//    public function startsWith($prefix): bool;
//
//    public function __wakeup();
//
//    public function __clone();
}

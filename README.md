# C33s Utils

```
PharHelper::isPhar()
PharHelper::directory()
```

deprecated use php8 (symfony/polyfill-php80) instead
```
StringHelper::isRegex($string)
StringHelper::startsWith($haystack, $needle)
StringHelper::endsWith($haystack, $needle)
```

<?php

declare(strict_types=1);

use AspectMock\Kernel;

// This is unit bootstrap for autoloading

$kernel = Kernel::getInstance();
$kernel->init([
    'debug' => true,
    'cacheDir' => __DIR__.'/../_data/cache',
    'includePaths' => [__DIR__.'/../../src']
]);

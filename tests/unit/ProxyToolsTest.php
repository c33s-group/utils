<?php

declare(strict_types=1);

namespace Tests\Unit;

use C33s\Utils\ProxyTools;
use AspectMock\Test as test;
use Codeception\Test\Unit;

/**
 * @group proxy
 */
class ProxyToolsTest extends Unit
{
    public function _before(): void
    {
        //unset OS proxy vars
        putenv("HTTP_PROXY");
        putenv("HTTPS_PROXY");
        putenv("NO_PROXY");
    }

    public function _after(): void
    {
        test::clean();
    }

    /**
     * @dataProvider getPaths
     */
    public function testProxyAwareFileGetContents($path, $proxy, $noProxy, $expected, $context = null): void
    {
        putenv($proxy);
        putenv($noProxy);

        $proxyTools = test::double(ProxyTools::class, ['fileGetContents' => 'noop']);
        self::assertEquals('noop', ProxyTools::proxyAwareFileGetContents($path, $context));
        $proxyTools->verifyInvoked('fileGetContents');
        $params = $proxyTools->getCallsForMethod('fileGetContents');
        $actual = $params[0][1];
        if (is_resource($actual) && 'stream-context' === get_resource_type($actual)) {
            $actual = stream_context_get_options($actual);
        }
        self::assertEquals($expected, $actual);

//        test::func('C33s\Util', 'file_get_contents', 'noop');
//        self::assertEquals('noop', ProxyTools::proxyAwareFileGetContents($path, $context));
    }

    /**
     * @dataProvider getProxyData
     */
    public function testGetProxy($proxy, $expected, $noProxy = null, $path = null): void
    {
        putenv("HTTPS_PROXY=$proxy");
        putenv("NO_PROXY=\"$noProxy\"");
        $actual = ProxyTools::getProxy($path);

        self::assertEquals($expected, $actual);
    }

    public function getProxyData(): iterable
    {
        yield 'returns null on no or empty proxy var' => ['proxy' => '',  null];
        yield 'proxy with http scheme' => ['proxy' => 'http://proxy.example.com',  'proxy.example.com'];
        yield 'proxy with http scheme and port' => ['proxy' => 'http://proxy.example.com:1040',  'proxy.example.com:1040'];
        yield 'proxy with https scheme and port' => ['proxy' => 'https://proxy.example.com:1040',  'proxy.example.com:1040'];
    }

    public function getPaths(): array
    {
        return [
            ['https://private.com/foo/README.md', 'HTTPS_PROXY=https://proxy.example.com:1080', 'NO_PROXY="127.0.0.1,private.com"',
                null],
            ['https://private.com/foo/README.md', 'HTTPS_PROXY=https://proxy.example.com:1080', 'NO_PROXY=',
                ['http' => ['proxy' => "tcp://proxy.example.com:1080"]]],
            ['https://private.com/foo/README.md', 'HTTPS_PROXY=https://proxy.example.com:1080', 'NO_PROXY=www.private.com',
                null],
            ['https://private.com/foo/README.md', 'HTTP_PROXY=http://proxy.example.com:1080', 'NO_PROXY=',
                null],
            ['http://private.com/foo/README.md', 'HTTP_PROXY=http://proxy.example.com:1080', 'NO_PROXY=',
                ['http' => ['proxy' => "tcp://proxy.example.com:1080"]]],
        ];
    }
}

<?php

declare(strict_types=1);

namespace Tests\Unit\String;

use C33s\Utils\String\UnicodeString;
use Codeception\Test\Unit;
use function C33s\Utils\String\u;

final class UnicodeStringFunctionTest extends Unit
{
    public function testFunctionAvailable(): void
    {
        $string = u('example');

        self::assertInstanceOf(UnicodeString::class, $string);
    }
}

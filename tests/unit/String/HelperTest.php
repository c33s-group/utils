<?php

declare(strict_types=1);

namespace Tests\Unit\String;

use C33s\Utils\String\Helper as StringHelper;
use Codeception\Test\Unit;

class HelperTest extends Unit
{
    /**
     * @dataProvider getStringsForIsRegex
     */
    public function testIsRegex($string, $expected, $char = null): void
    {
        if (is_string($char)) {
            self::assertEquals($expected, StringHelper::isRegex($string, $char));
        } else {
            self::assertEquals($expected, StringHelper::isRegex($string));
        }
    }

    public function getStringsForIsRegex(): array
    {
        return [
            ['/dummy/', true],
            ['/./', true],
            ['/-/', true],
            ['abc', false],
            ['1', false],
            ['/[a-zA-Z0-9]+/', true],
            ['@[a-zA-Z0-9]+@', true, '@'],
            ['[a-zA-Z0-9]+', false],
            ['[a-zA-Z0-9]+', false, '@'],
            ['/[a-zA-Z0-9]+/', false, '@'],
            ['[a-zA-Z0-9]+', false, ''],
        ];
    }

    /**
     * @dataProvider getStringsForStartWith
     */
    public function testStartsWith($string, $needle, $expected): void
    {
        self::assertEquals($expected, StringHelper::startsWith($string, $needle));
    }

    public function getStringsForStartWith(): array
    {
        return [
            ['/example', '/', true],
            ['e/xample', '/', false],
            ['ex/ample', '/', false],
            ['exa/mple', '/', false],
            ['example/', '/', false],
            [' example', ' ', true],
            ['example', 'e', true],
            ['example', 'ex', true],
            ['example', 'example', true],
            ['example', 'examples', false],
        ];
    }

    /**
     * @dataProvider getStringsForEndWith
     */
    public function testEndsWith($string, $needle, $expected): void
    {
        self::assertEquals($expected, StringHelper::endsWith($string, $needle));
    }

    public function getStringsForEndWith(): array
    {
        return [
            ['example/', '/', true],
            ['exampl/e', '/', false],
            ['examp/le', '/', false],
            ['exa/mple', '/', false],
            ['/example', '/', false],
            ['example ', ' ', true],
            ['example', 'e', true],
            ['example', 'le', true],
            ['example', 'example', true],
            ['example', 'examples', false],
        ];
    }

    /**
     * @dataProvider getStringsForContains
     */
    public function testContains($string, $needle, $expected): void
    {
        self::assertEquals($expected, StringHelper::contains($string, $needle));
    }

    public function getStringsForContains(): array
    {
        return [
            ['example', '/', false],
            ['example', 'e', true],
            ['example', 'x', true],
            ['example', 'l', true],
            ['example', 'exa', true],
            ['example', 'eme', false],
            ['example', 'examples', false],
        ];
    }
}
